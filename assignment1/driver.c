#ifndef DRIVER_C
#define DRIVER_C

#include "list.h"
#include <stdio.h>

void free(void* toFree){
    return;
}

bool equal(void* itemToCheck, void* itemToFind){
    printf("in equal: %d, %d\n", *(int*)itemToCheck, *(int*)itemToFind);
    if(*(int*)itemToCheck == *(int*)itemToFind){
        return true;
    }
    else{
        return false;
    }
}

int main(){
    // test that after using all LIST heads returns NULL and after free all lists correctly
    struct LIST* maxCheck[11];
    for(int i = 0; i <= 10; i++){
        maxCheck[i] = ListCreate();
    }
    if(maxCheck[10] == NULL){
        printf("Max LIST head creation returned NULL after becoming full\n");
    }
    for(int i = 0; i <= 10; i++){
        ListFree(maxCheck[i], &free);
    }
    printf("All lists deleted\n");

    // test ListConcat and also randomly other functions while adding nodes
    struct LIST* list =  ListCreate();
    int data0 = 0;
    int data1 = 1;
    int data2 = 2;
    int data3 = 3;
    int check1 = ListAdd(list, &data2);
    int check2 = ListAppend(list, &data3);
    int check3 = ListPrepend(list, &data0);
    printf("Check 1: %i\n", check1);
    printf("Check 2: %i\n", check2);
    printf("Check 3: %i\n", check3);

    void* listCurrCheck = ListCurr(list);
    printf("Current: %d\n", *(int*)listCurrCheck);

    int listCount = ListCount(list);
    printf("Count is: %d\n", listCount);

    void* listFirst = ListFirst(list);
    printf("First: %d\n", *(int*)listFirst);

    void* listLast = ListLast(list);
    printf("Last: %d\n", *(int*)listLast);

    void* listPrev = ListPrev(list);
    printf("Prev: %d\n", *(int*)listPrev);

    listPrev = ListPrev(list);
    printf("Prev: %d\n", *(int*)listPrev);

    void* listNext = ListNext(list);
    printf("Next: %d\n", *(int*)listNext);

    int check0 = ListInsert(list, &data1);
    printf("Check 0: %i\n", check0);

    listCurrCheck = ListCurr(list);
    printf("Current: %d\n", *(int*)listCurrCheck);

    listNext = ListNext(list);
    printf("Next: %d\n", *(int*)listNext);

    listCurrCheck = ListCurr(list);
    printf("Current: %d\n", *(int*)listCurrCheck);

    void* listRemove = ListRemove(list);
    printf("Remove: %d\n\n", *(int*)listRemove);

    listCurrCheck = ListCurr(list);
    printf("Current: %d\n", *(int*)listCurrCheck);

    listPrev = ListPrev(list);
    printf("Prev: %d\n", *(int*)listPrev);

    struct LIST* list2 = ListCreate();
    int data5 = 5;
    int data6 = 6;
    int data7 = 7;

    listNext = ListNext(list2);
    if(listNext == NULL){
        printf("List2: NULL is correct\n");
    }

    int check5 = ListAdd(list2, &data5);
    int check6 = ListAppend(list2, &data6);
    int check7 = ListPrepend(list2, &data7);
    printf("Check 5: %i\n", check5);
    printf("Check 6: %i\n", check6);
    printf("Check 7: %i\n", check7);

    listCurrCheck = ListCurr(list2);
    printf("Current: %d\n", *(int*)listCurrCheck);

    listCount = ListCount(list2);
    printf("Count is: %d\n", listCount);

    listFirst = ListFirst(list2);
    printf("First: %d\n", *(int*)listFirst);

    listLast = ListLast(list2);
    printf("Last: %d\n", *(int*)listLast);

    listPrev = ListPrev(list2);
    printf("Prev: %d\n", *(int*)listPrev);

    listPrev = ListPrev(list2);
    printf("Prev: %d\n", *(int*)listPrev);

    listNext = ListNext(list2);
    printf("Next: %d\n", *(int*)listNext);

    listCount = ListCount(list);
    printf("Count of list is: %d\n", listCount);

    listCount = ListCount(list2);
    printf("Count of list2 is: %d\n", listCount);

    ListConcat(list, list2);
    printf("Concat done\n");
    
    listLast = ListLast(list);
    printf("Last: %d\n", *(int*)listLast);

    listCount = ListCount(list);
    printf("Count is: %d\n", listCount);

    void* listTrim = ListTrim(list);
    printf("Trim is: %d\n", *(int*)listTrim);

    listCurrCheck = ListCurr(list);
    printf("Current: %d\n", *(int*)listCurrCheck);

    listCount = ListCount(list);
    printf("Count is: %d\n", listCount);

    // print all nodes
    // make currentItem be before the list so a series of ListNext will print the list
    ListFirst(list); 
    ListPrev(list);
    for(int i = 0; i <= listCount; i++){
        listNext = ListNext(list);
        if(i == listCount){
            if(listNext == NULL){
                printf("Last one is NULL while printing\n");
            }
        }
        else{
            printf("Printing whole list %d: %d\n",i , *(int*)listNext);
        }
    }
    
    
    // list search, should have int 1 and should not have int 2 as it was removed above
    // make currentItem the begining
    printf("1\n");
    ListFirst(list);
    void* search = ListSearch(list, &equal, &data5);
    if(search == NULL){
        printf("Search: not found\n");
    }
    else{
        printf("Search: %d\n", *(int*)search);
    }

    search = ListSearch(list, &equal, &data1);
    if(search == NULL){
        printf("Search: not found\n");
    }
    else{
        printf("Search: %d\n", *(int*)search);
    }

    search = ListSearch(list, &equal, &data6); // data came from list2 before concat
    if(search == NULL){
        printf("Search: not found\n");
    }
    else{
        printf("Search: %d\n", *(int*)search);
    }

    // make currentItem the begining
    ListFirst(list);
    search = ListSearch(list, &equal, &data2); // should not be found
    if(search == NULL){
        printf("2 was not found in the list\n");
    }
    else{
        printf("Search: %d\n", *(int*)search);
    }

    // deleted concat list correctly, the list variable holds an address but it doesn't point to a list
    ListFree(list, &free);

    listCurrCheck = ListCurr(list);
    if(listCurrCheck == NULL){
        printf("After ListFree, list is NULL\n");
    }

    // check all nodes were freed
    list = ListCreate();
    for(int i = 0; i <= 10; i++){ // should print 10 (index i) when i = 10 as there are only 10 nodes and this is the 11th ListAdd
        check2 = ListAdd(list, &data2);
        if(check2 == -1){
            printf("%d\n", i);
        }
    }

    ListFree(list, &free); // delete list, the list variable holds an address but it doesn't point to a list

    // end of driver and testing
    return 0;
}

#endif
