#ifndef S_TALK
#define S_TALK


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "list.h"

// gets keyboard inputs
void* keyboard(void* item);

// sends message through socket
void* sender(void* item);

// outputs to screen
void* screen(void* item);

// receives from socket
void* receiver(void* item);

// used in ListFree()
void free(void* item);

#endif