#include "s-talk.h"

// there will be 2 lists, 1 for sending and 1 for receiving. Each list is in a structure with a mutex for sync.
// For send list it will be syncing thread keyboard and send. For recieved list it will be syncing screen and recv thread.
struct ListStruct{
    pthread_mutex_t listMutex;
    struct LIST* list;
};

struct ListStruct received; // global ListStruct for recieved 
struct ListStruct sent; // global ListStruct for send 

// condition variables for buffer space
pthread_cond_t receivedCond;
pthread_cond_t sendCond;

int sock; // socket to get messages on
struct addrinfo* remoteAddr;
struct addrinfo* myAddr;

// threads
pthread_t keyboardThread;
pthread_t sendThread;
pthread_t screenThread;
pthread_t recvThread;

// constants
#define MAX_LENGTH 512 // max length of message
#define MAX_BUFFER 256 // max length of the message buffer

bool endChat;

int main(int argc, char* argv[]){
    // check argc == 4 or else the number of parameters passed in is incorrect
    if(argc != 4){
        printf("Not enough parameters\n");
        return(-1);
    }
    
    // extract the parameters passes in
    char* myPort = argv[1];
    char* remoteName = argv[2];
    char* remotePort = argv[3];
    
    // check port numbers are in the correct bounds of >1024 and <65535
    if(atoi(myPort) < 1024 || atoi(myPort) > 65535){
        printf("Invalid personal port number\n");
        return -1;
    }
    if(atoi(remotePort) < 1024 || atoi(remotePort) > 65535){
        printf("Invalid remote port number\n");
        return -1;
    }
    
    // socket setup
    struct sockaddr_in socketAddr; // socket info
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == 0){ 
        printf("Creating socket failed\n");
        return -1;
    }
    
    // fill out socket info
    socketAddr.sin_family = AF_INET;
    socketAddr.sin_port = htons(atoi(myPort));
    socketAddr.sin_addr.s_addr = INADDR_ANY;
    memset(&socketAddr.sin_zero, 0, 8);
    
    // bind socket to myPort
    if (bind(sock, (struct sockaddr *)&socketAddr, sizeof(struct sockaddr_in))<0){ 
        printf("Socket bind failed\n");
        close(sock);
        return -1; 
    }
    
    // set up peers socket info to send messages on
    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    
    if(getaddrinfo(remoteName, remotePort, &hints, &remoteAddr) != 0){
        printf("Getting remote machine address failed\n");
        close(sock);
        return -1;
    }
    
    // initialize received condition variable
    if(pthread_cond_init(&receivedCond, NULL) != 0){
        printf("Initializing received condition failed\n");
        close(sock); // close socket
        return -1;
    }
    
    // initialize send condition variable
    if(pthread_cond_init(&sendCond, NULL) != 0){
        printf("Initializing received condition failed\n");
        pthread_cond_destroy(&receivedCond);
        close(sock); // close socket
        return -1;
    }
    
    // create the received list
    received.list = ListCreate();
    if(received.list == NULL){
        printf("Creating received list failed\n");
        close(sock); // close socket
        pthread_cond_destroy(&receivedCond);
        pthread_cond_destroy(&sendCond);
        return -1;
    }
    // init mutex for recieved list
    if(pthread_mutex_init(&received.listMutex, NULL) != 0){
        printf("Initializing received list mutex failed\n");
        ListFree(received.list, &free);
        close(sock); // close socket
        pthread_cond_destroy(&receivedCond);
        pthread_cond_destroy(&sendCond);
        return -1;
    }
    // create the send list
    sent.list = ListCreate();
    if(sent.list == NULL){
        printf("Creating send list failed\n");
        ListFree(received.list, &free);
        close(sock); // close socket
        pthread_cond_destroy(&receivedCond);
        pthread_cond_destroy(&sendCond);
        return -1;
    }
    // init mutex for send list
    if(pthread_mutex_init(&sent.listMutex, NULL) != 0){
        printf("Initializing send list mutex failed\n");
        ListFree(received.list, &free);
        ListFree(sent.list, &free);
        close(sock); // close socket
        pthread_cond_destroy(&receivedCond);
        pthread_cond_destroy(&sendCond);
        return -1;
    }
    
    endChat = false;
    
    // create threads to run the functions
    pthread_create(&sendThread, NULL, sender, NULL);
    pthread_create(&recvThread, NULL, receiver, NULL);
    pthread_create(&keyboardThread, NULL, keyboard, NULL);  
    pthread_create(&screenThread, NULL, screen, NULL);


    // join the threads back as they finish up
    pthread_join(keyboardThread, NULL);
    pthread_join(sendThread, NULL);
    pthread_join(screenThread, NULL);
    pthread_join(recvThread, NULL);
    
    // clean up
    ListFree(received.list, &free); // free list, no dynamic memory used so the free function does not have to be anything
    pthread_mutex_destroy(&received.listMutex); // destroy mutex at the end
    ListFree(sent.list, &free); // free list, no dynamic memory used so the free function does not have to be anything
    pthread_mutex_destroy(&sent.listMutex); // destroy mutex at the end
    pthread_cond_destroy(&receivedCond); // destroy the condition variable
    pthread_cond_destroy(&sendCond); // destroy the condition variable
    close(sock); // close socket

    return 0;
}

void* keyboard(void* item){
    char* message = (char*) malloc(MAX_LENGTH * sizeof(char));
    while(1){
        fgets(message, MAX_LENGTH, stdin); // read keyboard input
        pthread_mutex_lock(&sent.listMutex); // C.S. adding to the send list
        
        // if send list is full, wait till there is space
        if(ListCount(sent.list) == MAX_BUFFER){
            pthread_cond_wait(&sendCond, &sent.listMutex);
        }
        ListAppend(sent.list, message); // add the message to the send list to be sent in the other thread
        pthread_cond_signal(&sendCond); // signal something has been added to the send list
        pthread_mutex_unlock(&sent.listMutex); // C.S. done
        
        if(endChat == true){
            break;
        }        
        
        // check for end chat message input of !
        if(message[0] == '!'){
            break;
        }        
    }
    printf("keyboard exited\n");
    pthread_exit(NULL);
}

void* sender(void* item){
    while(1){
        pthread_mutex_lock(&sent.listMutex); // C.S. sending to the remote from send list
        
        // if send list is empty, wait till something is added to it
        if(ListCount(sent.list) == 0){
            pthread_cond_wait(&sendCond, &sent.listMutex);
        }
        
        char* message = (char*)ListTrim(sent.list); // trim the last message from the send list
        
        if(endChat == true){
            free(message);
            break;
        }
        
        pthread_cond_signal(&sendCond); // signal space in the send list
        pthread_mutex_unlock(&sent.listMutex); // C.S. done
        
        if(sendto(sock, message, MAX_LENGTH, 0, remoteAddr->ai_addr, remoteAddr->ai_addrlen) == -1){
            printf("Console: Error sending message %s\n", message);
        }
        
        // check for end chat message input of !
        if(message[0] == '!' && message[1] == '\n'){
            endChat = true;
            pthread_cond_signal(&receivedCond); // ending chat signal the waiting threads nothing is coming
            pthread_cancel(recvThread);
            printf("receiver exited\n");
            free(message);
            break;
        }
    }
    printf("sender exited\n");
    pthread_exit(NULL);
}

void* screen(void* item){
    while(1){
        pthread_mutex_lock(&received.listMutex); // C.S. accessing the receieve list
        
        // if receieve list is empty, wait till something is added to it
        if(ListCount(received.list) == 0){
            pthread_cond_wait(&receivedCond, &received.listMutex);
        }

        char* message = (char*)ListTrim(received.list); // trim the last message from the received list

        pthread_cond_signal(&receivedCond); // signal space in the received list
        pthread_mutex_unlock(&received.listMutex); // C.S. done
        
        if(endChat == true){
            free(message);
            break;
        }
        
        // check for end chat message input of !
        if(message[0] == '!' && message[1] == '\n'){
            endChat = true;
            pthread_cond_signal(&sendCond); // ending chat signal the waiting threads nothing is coming
            pthread_cancel(keyboardThread);
            printf("keyboard exited\n");
            free(message);
            break;
        }
        printf("Remote: %s\n", message);
        free(message);
    }
    printf("screen exited\n");
    pthread_exit(NULL);
}

void* receiver(void* item){
    struct sockaddr_storage srcAddr;
    socklen_t remoteLength = sizeof(srcAddr);
    
    char* message = (char*) malloc(MAX_LENGTH * sizeof(char));
    
    while(1){
        if(recvfrom(sock, message, MAX_LENGTH, 0, (struct sockaddr*)&srcAddr, &remoteLength) > 0){
            pthread_mutex_lock(&received.listMutex); // C.S. accessing the receieve list
            
            // if receieve list is full, wait till there is space
            if(ListCount(received.list) == MAX_BUFFER){
                pthread_cond_wait(&receivedCond, &received.listMutex);
            }

            ListAppend(received.list, message); // add the message to the received list to be output to the screen in the other thread
            
            pthread_cond_signal(&receivedCond); // signal space in the received list
            pthread_mutex_unlock(&received.listMutex); // C.S. done
            
            if(endChat == true){
                break;
            }
            
            // check for end chat message input of !
            if(message[0] == '!' && message[1] == '\n'){
                printf("Console: Recieved end chat command\n");
                break;
            }
            
            message = (char*) malloc(MAX_LENGTH * sizeof(char));
        }
        else{
            printf("Console: no message received\n");
        }
    }
    printf("receiver exited\n");
    pthread_exit(NULL);
}

void free(void* toFree){
    return;
}