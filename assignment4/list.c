#ifndef LIST_H
#define LIST_H

#include "list.h"
#include <stdbool.h>
#include <stdio.h>

struct node { //Holds one data item of each list. Basic building block of a LIST. NODE ARRAY
    struct node* next;
    struct node* previous;

    //int next; // Array index of the next node
    //int previous; // Array index of thee previous node

    void* data;
};

struct LIST { //Contain the metadata of the LIST, HEAD ARRAY
    int count; // number of nodes in the list

    struct node* head;
    struct node* tail;
    struct node* currentItem;
    struct LIST* next; // next empty LIST in headArray

    //int head; // Array index of the head of the list
    //int tail; // Array index of the tail of the list
    //int currentItem; // Array index for the current item of the list
    
    int state; // -1 = before the list, 0 = in the list, 1 = past the list 
};

#define headArraySize 100
#define nodeArraySize 1000
#define listOfEmptyNodes headArray[0]

// first index of headArray is a list of empty nodes. When an empty node is needed take it 
// from the front. Add a reclaimed node by adding it to the back
struct LIST headArray[headArraySize + 1]; // add one becuase the first index is listOfEmptyNodes
struct node nodeArray[nodeArraySize];

struct LIST* ListCreate(){

    static bool initialized = false;
    if(initialized == false){
        listOfEmptyNodes.head = nodeArray;
 
        // edge case, set the first node 
        nodeArray[0].previous = NULL;
        nodeArray[0].next = &nodeArray[1];
        nodeArray[0].data = NULL;

        listOfEmptyNodes.tail = &nodeArray[nodeArraySize - 1];
        // edge case, set the last node
        nodeArray[nodeArraySize - 1].previous = &nodeArray[nodeArraySize - 2];
        nodeArray[nodeArraySize - 1].next = NULL;
        nodeArray[nodeArraySize - 1].data = NULL;

        // edge case, set the last headArray
        headArray[headArraySize].next = NULL; // [headArraySize + 1 - 1]
        headArray[headArraySize].head = NULL;
        headArray[headArraySize].tail = NULL;
        headArray[headArraySize].currentItem = NULL;
        headArray[headArraySize].state = 0;

        listOfEmptyNodes.currentItem = NULL; // not relevant for this list
        listOfEmptyNodes.state = 0; // not relevant for this list
        listOfEmptyNodes.count = 2; // first and last nodes already set

        // set all remaining nodeArray
        for (int i = 1; i < nodeArraySize - 1; i++){
            nodeArray[i].previous = &nodeArray[i - 1];
            nodeArray[i].next = &nodeArray[i + 1];
            nodeArray[i].data = NULL;
            listOfEmptyNodes.count = listOfEmptyNodes.count + 1;
        }
        
        // set all remaining headArray
        for (int i = 0; i < headArraySize; i++){ // [(headArraySize + 1) - 1]
            headArray[i].next = &headArray[i + 1];
            if(i != 0){
                headArray[i].head = NULL;
                headArray[i].tail = NULL;
            }
            headArray[i].currentItem = NULL;
            headArray[i].state = 0;
        }
        initialized = true; // initialization will only run once
    }
    if(headArray[0].next != NULL){ // at least one LIST head left
        
        struct LIST* newHead = headArray[0].next; // find an empty LIST in headArray
        if(newHead->next == NULL){ // only 1 LIST head left
            headArray[0].next = NULL; //
        }
        else{
            headArray[0].next = newHead->next; // remove empty LIST from list 
        }

        // initialize new LIST head
        newHead->head = NULL;
        newHead->tail = NULL;
        newHead->currentItem = NULL;
        newHead->next = NULL; // not relevant while part of active list
        newHead->state = -1; /* is this the correct state?*/ // 1/29/2020 asked prof and he said -1 was sensible
        newHead->count = 0;

        return newHead;
    }
    else{
        return NULL; // no empty LIST heads
    }
}

int ListCount(struct LIST* list){
    if(list == NULL){
      return 0;
    }
    return list->count;
}

void* ListFirst(struct LIST* list){
    if(list == NULL){
      return NULL;
    }
    // check if the list is empty or not
    if(list->count != 0){
        list->currentItem = list->head; // update currentItem
        list->state = 0; // update state to being in the list
        return list->head->data;
    }
    else{
        return NULL; // list is empty
    }  
}

void* ListLast(struct LIST* list){
    if(list == NULL){
      return NULL;
    }
    // check if list is empty
    if(list->count != 0){
        list->currentItem = list->tail; // update currentItem
        list->state = 0; // update state to being in the list
        return list->tail->data;
    }
    else{
        return NULL; // list is empty
    }
}

void* ListNext(struct LIST* list){
    if(list == NULL){
      return NULL;
    }
    if(list->currentItem != NULL && list->state == 0){ // check not at the end of the list
        if(list->currentItem->next == NULL){
            list->currentItem = NULL;
            list->state = 1; // currentItem has advanced beyond the end of the list
            return NULL;
        }
        else{
          list->currentItem = list->currentItem->next; // update currentItem
          return list->currentItem->data;
        }
    }
    else if(list->state == 1){ // further beyond, no where to go
        return NULL;
    }
    else if(list->state == -1){ // before the start of the list thus move to the start
        if(list->count == 0){ // list is empty so just flip state
            list->currentItem = NULL;
            list->state = 1;
            return NULL;
        }
	      else{
          list->currentItem = list->head;
          list->state = 0; // update state to being in the list
          return list->currentItem->data;
        }
    }
    else{ // at the end of the list
        list->currentItem = NULL;
        list->state = 1; // currentItem has advanced beyond the end of the list
        return NULL;
    }
}

void* ListPrev(struct LIST* list){
    if(list == NULL){
      return NULL;
    }
    if(list->currentItem != NULL && list->state == 0){ // check not at the end of the list
        if(list->currentItem->previous == NULL){
            list->currentItem = NULL;
            list->state = -1;
            return NULL;
        }
        else{
            list->currentItem = list->currentItem->previous; // update currentItem
            return list->currentItem->data;
        }
    }
    else if(list->state == -1){ //before the list, no where to go
        return NULL;
    }
    else if(list->state == 1){ // beyond the end of the list thus move to the end
        if(list->count == 0){ // list is empty so just flip state
            list->currentItem = NULL;
            list->state = -1;
            return NULL;
        }
        else{
          list->currentItem = list->tail;
          list->state = 0; // update state to being in the list
          return list->currentItem->data;
        }
    }
    else{ // at the begining of the list
        list->currentItem = NULL;
        list->state = -1; // currentItem moved before the start of the list
        return NULL;
    }
}

void* ListCurr(struct LIST* list){
    if(list == NULL){
        return NULL;
    }
    if(list->currentItem != NULL){
        return list->currentItem->data;
    }
    else{
        return NULL;
    }
}

int ListAdd(struct LIST* list, void* item){
    if(list == NULL){
        return -1;
    }
    if(listOfEmptyNodes.count == 0){ // no empty nodes available, failure
        return -1;
    }
    // remove node out of the listOfEmptyNodes
    struct node* newNode = listOfEmptyNodes.head;
    if(listOfEmptyNodes.count == 1){
        listOfEmptyNodes.head = NULL;
        listOfEmptyNodes.tail = NULL;
    }
    else{
    listOfEmptyNodes.head = newNode->next;
    listOfEmptyNodes.head->previous = NULL; // will not be used while in listOfEmptyNodes but still make it NULL
    }
    listOfEmptyNodes.count = listOfEmptyNodes.count - 1; // took a node out, decrement
    newNode->data = item; // set up the newNode

    // insert into the list depending on the state
    if(list->state == -1){ // add to the head
        if(list->count == 0){ // empty, new node will be both head and tail
            newNode->next = NULL;
            newNode->previous = NULL;
            list->head = newNode;
            list->tail = newNode;
            list->currentItem = newNode;
            list->state = 0;
        }
        else{
            newNode->next = list->head;
            newNode->previous = NULL;
            list->head->previous = newNode;
            list->head = newNode;
            list->currentItem = newNode;
            list->state = 0;
        }
    }
    else if(list->state == 1){ // add to the tail
        if(list->count == 0){ // empty, new node will be both head and tail
            newNode->next = NULL;
            newNode->previous = NULL;
            list->head = newNode;
            list->tail = newNode;
            list->currentItem = newNode;
            list->state = 0;
        }
        else{
            newNode->next = NULL;
            newNode->previous = list->tail;
            list->tail->next = newNode;
            list->tail = newNode;
            list->currentItem = newNode;
            list->state = 0;
        }
    }
    else{ // add to the currentItem
        if(list->currentItem = list->tail){ // new tail, update tail after adding. No need to update head even if only 1 node
            newNode->next = NULL;
            newNode->previous = list->currentItem;
            list->currentItem->next = newNode;
            list->currentItem = newNode;
            list->tail = newNode;
            // state = 0 to get here, no need to update            
        }
        else{
            newNode->next = list->currentItem->next;
            newNode->previous = list->currentItem;
            list->currentItem->next = newNode;
            list->currentItem->next->previous = newNode;
            list->currentItem = newNode;
            // state = 0 to get here, no need to update
        }
    }

    list->count = list->count + 1; // another node added to the list, increment
    return 0;
}

int ListInsert(struct LIST* list, void* item){
    if(list == NULL){
        return -1;
    }
    if(listOfEmptyNodes.count == 0){ // no empty nodes available, failure
        return -1;
    }

    // insert into the list depending on the state
    if(list->state == -1 || list->state == 1){ // this is the same as ListAdd so just call ListAdd
        return ListAdd(list, item);
    }
    else{ // not the same as ListAdd, put newNode code in here. Add to the currentItem
        // remove node out of the listOfEmptyNodes
        struct node* newNode = listOfEmptyNodes.head;
        if(listOfEmptyNodes.count == 1){
            listOfEmptyNodes.head = NULL;
            listOfEmptyNodes.tail = NULL;
        }
        else{
            listOfEmptyNodes.head = listOfEmptyNodes.head->next;
            listOfEmptyNodes.head->previous = NULL; // will not be used while in listOfEmptyNodes but still make it NULL
        }
        
        listOfEmptyNodes.count = listOfEmptyNodes.count - 1; // took a node out, decrement
        newNode->data = item; // set up a newNode

        if(list->currentItem == list->head){ // new head, update head after adding. No need to update tail even if only 1 node
            newNode->previous = NULL;
            newNode->next = list->head;
            list->currentItem->previous = newNode;
            list->currentItem = newNode;
            list->head = newNode;
            // state = 0 to get here, no need to update  
        }
        else{
            newNode->next = list->currentItem;
            newNode->previous = list->currentItem->previous;
            list->currentItem->previous->next = newNode;
            list->currentItem->previous = newNode;
            list->currentItem = newNode;
            // state = 0 to get here, no need to update  
        }

        list->count = list->count + 1; // another node added to the list, increment
        return 0;
    }
}

int ListAppend(struct LIST* list, void* item){ // same as ListAdd with state = 1 or currentItem = list->tail
    if(list == NULL){
        return -1;
    }
    list->state = 1;
    return ListAdd(list, item);
}

int ListPrepend(struct LIST* list, void* item){ // same as ListInsert with state = -1 or curentItem = list->head
    if(list == NULL){
        return -1;
    }
    list->state = -1;
    return ListInsert(list, item);
}

void* ListRemove(struct LIST* list){
    if(list == NULL){
        return NULL;
    }
    if(list->count == 0 || list->currentItem == NULL){ // list is empty, nothing to remove or the currentItem pointer is NULL
        return NULL;
    }
    else{ // remove if empty update state. Edge cases: 1 node, currentItem is head or tail
        struct node* removeNode = list->currentItem;
        void* data = removeNode->data; // get a pointer to the data to return;
        removeNode->data = NULL; // remove the data for security

        if(list->count == 1){ // list will be empty after
            list->currentItem = NULL;
            list->state = -1;
            list->head = NULL;
            list->tail = NULL;
        }
        else if(list->currentItem == list->head){ // head and currentItem become next node. Tail and state do not need to be updated
            list->head = list->head->next;
            list->currentItem = list->head;
            list->head->previous = NULL;
        }
        else if(list->currentItem == list->tail){ // remove tail and set currentItem to NULL. Set state to 1 as currentItem has advanced past the end
            list->tail = list->tail->previous;
            list->currentItem = NULL;
            list->state = 1;
            list->tail->next = NULL;
        }
        else{
            list->currentItem->previous->next = list->currentItem->next;
            list->currentItem->next->previous = list->currentItem->previous;
            list->currentItem = list->currentItem->next;
        }

        // add the node to listOfEmptyNodes
        if(listOfEmptyNodes.count == 0){
            listOfEmptyNodes.head = removeNode;
            listOfEmptyNodes.tail = removeNode;
            removeNode->next = NULL;
            removeNode->previous = NULL;
        }
        else{
        removeNode->previous = listOfEmptyNodes.tail;
        removeNode->next = NULL;
        listOfEmptyNodes.tail->next = removeNode;
        listOfEmptyNodes.tail = removeNode;
        }
        listOfEmptyNodes.count = listOfEmptyNodes.count + 1; // another node added to listOfEmptyNodes, increment  

        list->count = list->count - 1; // a node was removed, decrement
        return data;
    }
}

void ListConcat(struct LIST* list1, struct LIST* list2){
    if(list2 == NULL){
        return;
    }
    else if(list1 == NULL){
        list1 = list2;
        list2 = NULL;
        return;
    }
    
    // if both lists are empty or list2 is empty then nothing needs to be done, just remove the LIST head and 
    //add it to the free LIST heads
    if(list1->count != 0 && list2->count != 0){ // both lists have nodes
        // connect to 2 lists together
        list1->tail->next = list2->head;
        list2->head->previous = list1->tail;
        list1->tail = list2->tail; // list1 now holds all the nodes of both lists
        list1->count += list2->count; // add the two counts together
        // currentItem and state remain the same
    }
    else if(list1->count == 0 && list2->count != 0){ // list1 is empty but list2 is not, swap the pointers
        struct LIST* temp = list1;
        list1 = list2;
        list2 = temp;
        list1->currentItem = NULL;
        list1->state = -1;
        // remove list2 head and add it to the free LIST heads
    }
    // add list2 head to the front of the list of empty LIST heads
    // delete all the info about the previous list for security
    list2->count = 0;
    list2->currentItem = NULL;
    list2->head = NULL;
    list2->tail = NULL;
    list2->state = -1;
    
    // slot in the front of the free LIST heads
    list2->next = headArray[0].next;
    headArray[0].next = list2;
    
    return;
}

void ListFree(struct LIST* list, void itemFree(void*)){
    if(list == NULL){
        return;
    }
    if(list->count != 0){ // call itemFree for each item
      list->currentItem = list->head;
      struct node* removeNode = list->currentItem;
      do{
          (*itemFree)(removeNode->data);
          ListRemove(list);
          removeNode = list->currentItem;
      }while(removeNode != NULL);
    }
    // delete all list info
    list->head = NULL;
    list->tail = NULL;
    list->state = 0;
    list->currentItem = NULL;

    // add list head back to empty LIST heads at the front, singly linked list
    list->next = headArray[0].next;
    headArray[0].next = list;
    list = NULL; // list no longer points to anything

    return;
}

void* ListTrim(struct LIST* list){
    if(list == NULL){
        return NULL;
    }
    if(list->count == 0){ // list empty nothing to trim
        return NULL;
    }

    struct node* trimNode = list->tail;
    void* data = trimNode->data; // get a pointer to the data to return;

    trimNode->data = NULL; // remove the data for security

    if(list->count == 1){ // list will be empty after this trim, set state -1
        // list is empty
        list->head = NULL;
        list->tail = NULL;
        list->state = -1; // update state for empty LIST
        list->currentItem = NULL;
    }
    else{ // trimming one of many nodes
        // update list
        list->currentItem = list->tail->previous;
        list->tail = list->tail->previous;
        list->tail->next = NULL;
    }

    // add the node to the tail of listOfEmptyNodes
    if(listOfEmptyNodes.count == 0){ // listOfEmptyNodes is empty, trimNode is both head and tail
        listOfEmptyNodes.head = trimNode;
        listOfEmptyNodes.tail = trimNode;
        trimNode->next = NULL;
        trimNode->previous = NULL;
    }
    else{
    trimNode->previous = listOfEmptyNodes.tail;
    trimNode->next = NULL;
    listOfEmptyNodes.tail->next = trimNode;
    listOfEmptyNodes.tail = trimNode;
    }
    listOfEmptyNodes.count = listOfEmptyNodes.count + 1; // another node added to listOfEmptyNodes, increment
    
    list->count = list->count - 1; // a node was trimmed, decrement
    return data;
}

// item is any C data type and when found I return it. If found it returns a pointer to that item but you already have the item in comparisonArg.
// Make more sense to return a bool or something
void* ListSearch(struct LIST* list, bool comparator(void*, void*), void* comparisonArg){
    if(list == NULL || list->count == 0 || list->state == 1){
        return NULL;
    }
    struct node* checkNode = NULL;
    if(list->state == -1){ // before the list so search the entire list
        checkNode = list->head;
    }
    else{ // search from currentItem
        checkNode = list->currentItem;
    }
    do{
        bool temp = (*comparator)(checkNode->data, comparisonArg);
        if((*comparator)(checkNode->data, comparisonArg) == true){ 
            list->currentItem = checkNode;
            list->state = 0;
            return checkNode->data;
        }
        checkNode = checkNode->next;
    }while(checkNode != NULL);
    // did not find it in the list
    list->currentItem = NULL;
    list->state = 1;
    return NULL;
}

#endif