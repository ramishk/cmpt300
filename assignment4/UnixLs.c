#include "UnixLs.h"

bool longList;
bool inode;

int main(int argc, char* argv[]){    
    bool optionsOver = false;
    longList = false;
    inode = false;

    DIR* directory;
    struct dirent* directoryEntry;

    char dash[1] = "-";

    struct LIST* directoryList = ListCreate();
    struct stat directoryStat;
    
    for(int i = 1; i < argc; i++){
        char* arg = argv[i];
        if(arg[0] == '-'){ // ls -argument l or i
            if(optionsOver == true){
                printf("Cannot have options after directory arguments\n");
                return -1;
            }
            if(strchr(arg, 'i')){
                inode = true;
            }
            if(strchr(arg, 'l')){
                longList = true;
            }
        }
        else{ // ls directory/file 
            optionsOver = true;
            ListAppend(directoryList, argv[i]);
        }
    }

    if(longList == true && inode == true){ // print inode number and long list -li
        if(ListCount(directoryList) == 0){
            directory = opendir(".");
            if(directory == NULL){
                printf("ls: cannot access \'.\': No such file or directory\n");
                return -1;
            }
            directoryEntry = readdir(directory);
            while(directoryEntry != NULL){
                char* name = directoryEntry->d_name;
                if(lstat(name, &directoryStat) == -1){
                    printf("ls: cannot access file\n");
                    directoryEntry = readdir(directory);
                    continue;
                }
                bool hasSpace = false;
                if(strchr(name, ' ')){
                    hasSpace = true;
                }
                if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                }
                else{
                    bool isLink = false;
                    printf("%-11lu ", directoryEntry->d_ino);
                    if(S_ISDIR(directoryStat.st_mode)){ // directory print d
                        printf("d");
                    }
                    else if (S_ISLNK(directoryStat.st_mode)) // symbolic print l
                    {
                        printf("l");
                        isLink = true;
                    }
                    else if(S_ISREG(directoryStat.st_mode)){ // regular file print -
                        printf("-");
                    }
                    
                    if((S_IRUSR & directoryStat.st_mode) == S_IRUSR){ // owner read permission print r
                        printf("r");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IWUSR & directoryStat.st_mode) == S_IWUSR){ // owner write permission print w
                        printf("w");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IXUSR & directoryStat.st_mode) == S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID){ // owner exe permission print x
                        printf("s");
                    }
                    else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID)
                    {
                        printf("S");
                    }
                    else if ((S_IXUSR & directoryStat.st_mode) == S_IXUSR)
                    {
                        printf("x");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IRGRP & directoryStat.st_mode) == S_IRGRP){ // group read permission print r
                        printf("r");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IWGRP & directoryStat.st_mode) == S_IWGRP){ // group write permission print w
                        printf("w");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IXGRP & directoryStat.st_mode) == S_IXGRP && (S_ISGID & directoryStat.st_mode) == S_ISGID){ // group exe permission print x
                        printf("s");
                    }
                    else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISGID & directoryStat.st_mode) == S_ISGID)
                    {
                        printf("S");
                    }
                    else if ((S_IXGRP & directoryStat.st_mode) == S_IXGRP)
                    {
                        printf("x");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IROTH & directoryStat.st_mode) ==  S_IROTH){ // other read permission print r
                        printf("r");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IWOTH & directoryStat.st_mode) == S_IWOTH){ // other write permission print w
                        printf("w");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IXOTH & directoryStat.st_mode) == S_IXOTH){ // other exe permission print x also print space
                        printf("x ");
                    }
                    else{
                        printf("- ");
                    }

                    printf("%-2lu ", directoryStat.st_nlink); // num hard links

                    getAndPrintUserName(directoryStat.st_uid); // user name
                    getAndPrintGroup(directoryStat.st_gid); // group name

                    printf("%5li ", directoryStat.st_size); // size

                    struct tm* modTime = gmtime(&directoryStat.st_mtime);
                        switch (modTime->tm_mon)
                        {
                        case 0:
                            printf("Jan ");
                            break;
                        case 1:
                            printf("Feb ");
                            break;
                        case 2:
                            printf("Mar ");
                            break;
                        case 3:
                            printf("Apr ");
                            break;
                        case 4:
                            printf("May ");
                            break;
                        case 5:
                            printf("Jun ");
                            break;
                        case 6:
                            printf("Jul ");
                            break;
                        case 7:
                            printf("Aug ");
                            break;
                        case 8:
                            printf("Sep ");
                            break;
                        case 9:
                            printf("Oct ");
                            break;
                        case 10:
                            printf("Nov ");
                            break;
                        case 11:
                            printf("Dec ");
                            break;
                        default:
                            break;
                        }
                        printf("%2i %i ", modTime->tm_mday, modTime->tm_year + 1900);
                        printf("%02i:%02i ", modTime->tm_hour, modTime->tm_min);

                    if(hasSpace == true){
                        printf("\'");
                    }
                    printf("%s", name);
                    if(hasSpace == true){
                        printf("\' ");
                    }
                    else{
                        printf(" ");
                    }
                    if(isLink == true){
                        char filePath[PATH_MAX];
                        char fullPath[PATH_MAX] = "./";
                        strcat(fullPath, name);
                            realpath(name, filePath);
                            printf("-> %s\n", filePath);
                    }
                    else{
                        printf("\n");
                    }
                }
                directoryEntry = readdir(directory);
            }
            closedir(directory);
        }
        else{
            int count = ListCount(directoryList);
            char* directoryToPrint = ListTrim(directoryList);
            while(directoryToPrint != NULL){
                directory = opendir(directoryToPrint);
                if(directory == NULL){ // doesn't exit
                    printf("ls: cannot access \'%s\': No such file or directory\n", directoryToPrint);
                    return -1;
                }
                if(count != 1){ // only print directory name if more than 1 directory passed in
                    printf("\n");
                    printf("%s:\n", directoryToPrint);
                }
                directoryEntry = readdir(directory);
                while(directoryEntry != NULL){
                    char* name = directoryEntry->d_name;
                    char fullPath[150];
                    strcpy(fullPath, directoryToPrint);
                    strcat(fullPath, "/");
                    strcat(fullPath, name);

                    if(lstat(fullPath, &directoryStat) == -1){
                        printf("ls: cannot access file\n");
                        directoryEntry = readdir(directory);
                        continue;
                    }
                    bool hasSpace = false;
                    if(strchr(name, ' ')){
                        hasSpace = true;
                    }
                    if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                    }
                    else{
                        bool isLink = false;
                        printf("%-11lu ", directoryEntry->d_ino);
                        if(S_ISDIR(directoryStat.st_mode)){ // directory print d
                            printf("d");
                        }
                        else if (S_ISLNK(directoryStat.st_mode)) // symbolic print l
                        {
                            printf("l");
                            isLink = true;
                        }
                        else if(S_ISREG(directoryStat.st_mode)){ // regular file print -
                            printf("-");
                        }
                    
                        if((S_IRUSR & directoryStat.st_mode) == S_IRUSR){ // owner read permission print r
                            printf("r");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IWUSR & directoryStat.st_mode) == S_IWUSR){ // owner write permission print w
                            printf("w");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IXUSR & directoryStat.st_mode) == S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID){ // owner exe permission print x
                            printf("s");
                         }
                        else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID)
                        {
                            printf("S");
                        }
                        else if ((S_IXUSR & directoryStat.st_mode) == S_IXUSR)
                        {
                            printf("x");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IRGRP & directoryStat.st_mode) == S_IRGRP){ // group read permission print r
                            printf("r");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IWGRP & directoryStat.st_mode) == S_IWGRP){ // group write permission print w
                            printf("w");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IXGRP & directoryStat.st_mode) == S_IXGRP && (S_ISGID & directoryStat.st_mode) == S_ISGID){ // group exe permission print x
                            printf("s");
                        }
                        else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISGID & directoryStat.st_mode) == S_ISGID)
                        {
                            printf("S");
                        }
                        else if ((S_IXGRP & directoryStat.st_mode) == S_IXGRP)
                        {
                            printf("x");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IROTH & directoryStat.st_mode) ==  S_IROTH){ // other read permission print r
                        printf("r");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IWOTH & directoryStat.st_mode) == S_IWOTH){ // other write permission print w
                            printf("w");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IXOTH & directoryStat.st_mode) == S_IXOTH){ // other exe permission print x also print space
                            printf("x ");
                        }
                        else{
                            printf("- ");
                        }

                        printf("%-2lu ", directoryStat.st_nlink); // num hard links

                        getAndPrintUserName(directoryStat.st_uid); // user name
                        getAndPrintGroup(directoryStat.st_gid); // group name

                        printf("%5li ", directoryStat.st_size); // size

                        struct tm* modTime = gmtime(&directoryStat.st_mtime);
                        switch (modTime->tm_mon)
                        {
                        case 0:
                            printf("Jan ");
                            break;
                        case 1:
                            printf("Feb ");
                            break;
                        case 2:
                            printf("Mar ");
                            break;
                        case 3:
                            printf("Apr ");
                            break;
                        case 4:
                            printf("May ");
                            break;
                        case 5:
                            printf("Jun ");
                            break;
                        case 6:
                            printf("Jul ");
                            break;
                        case 7:
                            printf("Aug ");
                            break;
                        case 8:
                            printf("Sep ");
                            break;
                        case 9:
                            printf("Oct ");
                            break;
                        case 10:
                            printf("Nov ");
                            break;
                        case 11:
                            printf("Dec ");
                            break;
                        default:
                            break;
                        }
                        printf("%2i %i ", modTime->tm_mday, modTime->tm_year + 1900);
                        printf("%02i:%02i ", modTime->tm_hour, modTime->tm_min);

                        if(hasSpace == true){
                            printf("\'");
                        }
                        printf("%s", name);
                        if(hasSpace == true){
                            printf("\' ");
                        }
                        else{
                            printf(" ");
                        }
                        if(isLink == true){
                            char filePath[PATH_MAX];
                            realpath(fullPath, filePath);
                            printf("-> %s\n", filePath);
                        }
                        else{
                            printf("\n");
                        }
                        fullPath[0] = '\0';
                    }
                    directoryEntry = readdir(directory);
                }
                directoryToPrint = ListTrim(directoryList);
            }
            closedir(directory);
        }
    }
    else if(longList == true){ // print long list -l
        if(ListCount(directoryList) == 0){
            directory = opendir(".");
            if(directory == NULL){
                printf("ls: cannot access \'.\': No such file or directory\n");
                return -1;
            }
            directoryEntry = readdir(directory);
            while(directoryEntry != NULL){
                char* name = directoryEntry->d_name;
                if(lstat(name, &directoryStat) == -1){
                    printf("ls: cannot access file\n");
                    directoryEntry = readdir(directory);
                    continue;
                }
                bool hasSpace = false;
                if(strchr(name, ' ')){
                    hasSpace = true;
                }
                if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                }
                else{
                    bool isLink = false;
                    if(S_ISDIR(directoryStat.st_mode)){ // directory print d
                        printf("d");
                    }
                    else if (S_ISLNK(directoryStat.st_mode)) // symbolic print l
                    {
                        printf("l");
                        isLink = true;
                    }
                    else if(S_ISREG(directoryStat.st_mode)){ // regular file print -
                        printf("-");
                    }
                    
                    if((S_IRUSR & directoryStat.st_mode) == S_IRUSR){ // owner read permission print r
                        printf("r");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IWUSR & directoryStat.st_mode) == S_IWUSR){ // owner write permission print w
                        printf("w");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IXUSR & directoryStat.st_mode) == S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID){ // owner exe permission print x
                        printf("s");
                    }
                    else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID)
                    {
                        printf("S");
                    }
                    else if ((S_IXUSR & directoryStat.st_mode) == S_IXUSR)
                    {
                        printf("x");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IRGRP & directoryStat.st_mode) == S_IRGRP){ // group read permission print r
                        printf("r");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IWGRP & directoryStat.st_mode) == S_IWGRP){ // group write permission print w
                        printf("w");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IXGRP & directoryStat.st_mode) == S_IXGRP && (S_ISGID & directoryStat.st_mode) == S_ISGID){ // group exe permission print x
                        printf("s");
                    }
                    else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISGID & directoryStat.st_mode) == S_ISGID)
                    {
                        printf("S");
                    }
                    else if ((S_IXGRP & directoryStat.st_mode) == S_IXGRP)
                    {
                        printf("x");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IROTH & directoryStat.st_mode) ==  S_IROTH){ // other read permission print r
                        printf("r");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IWOTH & directoryStat.st_mode) == S_IWOTH){ // other write permission print w
                        printf("w");
                    }
                    else{
                        printf("-");
                    }

                    if((S_IXOTH & directoryStat.st_mode) == S_IXOTH){ // other exe permission print x also print space
                        printf("x ");
                    }
                    else{
                        printf("- ");
                    }

                    printf("%-2lu ", directoryStat.st_nlink); // num hard links

                    getAndPrintUserName(directoryStat.st_uid); // user name
                    getAndPrintGroup(directoryStat.st_gid); // group name

                    printf("%5li ", directoryStat.st_size); // size

                    struct tm* modTime = gmtime(&directoryStat.st_mtime);
                        switch (modTime->tm_mon)
                        {
                        case 0:
                            printf("Jan ");
                            break;
                        case 1:
                            printf("Feb ");
                            break;
                        case 2:
                            printf("Mar ");
                            break;
                        case 3:
                            printf("Apr ");
                            break;
                        case 4:
                            printf("May ");
                            break;
                        case 5:
                            printf("Jun ");
                            break;
                        case 6:
                            printf("Jul ");
                            break;
                        case 7:
                            printf("Aug ");
                            break;
                        case 8:
                            printf("Sep ");
                            break;
                        case 9:
                            printf("Oct ");
                            break;
                        case 10:
                            printf("Nov ");
                            break;
                        case 11:
                            printf("Dec ");
                            break;
                        default:
                            break;
                        }
                        printf("%2i %i ", modTime->tm_mday, modTime->tm_year + 1900);
                        printf("%02i:%02i ", modTime->tm_hour, modTime->tm_min);

                    if(hasSpace == true){
                        printf("\'");
                    }
                    printf("%s", name);
                    if(hasSpace == true){
                        printf("\' ");
                    }
                    else{
                        printf(" ");
                    }
                    if(isLink == true){
                        char filePath[PATH_MAX];
                        char fullPath[PATH_MAX] = "./";
                        strcat(fullPath, name);
                            realpath(name, filePath);
                            printf("-> %s\n", filePath);
                    }
                    else{
                        printf("\n");
                    }
                }
                directoryEntry = readdir(directory);
            }
            closedir(directory);
        }
        else{
            int count = ListCount(directoryList);
            char* directoryToPrint = ListTrim(directoryList);
            while(directoryToPrint != NULL){
                directory = opendir(directoryToPrint);
                if(directory == NULL){ // doesn't exit
                    printf("ls: cannot access \'%s\': No such file or directory\n", directoryToPrint);
                    return -1;
                }
                if(count != 1){ // only print directory name if more than 1 directory passed in
                    printf("\n");
                    printf("%s:\n", directoryToPrint);
                }
                directoryEntry = readdir(directory);
                while(directoryEntry != NULL){
                    char* name = directoryEntry->d_name;
                    char fullPath[150];
                    strcpy(fullPath, directoryToPrint);
                    strcat(fullPath, "/");
                    strcat(fullPath, name);

                    if(lstat(fullPath, &directoryStat) == -1){
                        printf("ls: cannot access file\n");
                        directoryEntry = readdir(directory);
                        continue;
                    }
                    bool hasSpace = false;
                    if(strchr(name, ' ')){
                        hasSpace = true;
                    }
                    if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                    }
                    else{
                        bool isLink = false;
                        if(S_ISDIR(directoryStat.st_mode)){ // directory print d
                            printf("d");
                        }
                        else if (S_ISLNK(directoryStat.st_mode)) // symbolic print l
                        {
                            printf("l");
                            isLink = true;
                        }
                        else if(S_ISREG(directoryStat.st_mode)){ // regular file print -
                            printf("-");
                        }
                    
                        if((S_IRUSR & directoryStat.st_mode) == S_IRUSR){ // owner read permission print r
                            printf("r");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IWUSR & directoryStat.st_mode) == S_IWUSR){ // owner write permission print w
                            printf("w");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IXUSR & directoryStat.st_mode) == S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID){ // owner exe permission print x
                            printf("s");
                         }
                        else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISUID & directoryStat.st_mode) == S_ISUID)
                        {
                            printf("S");
                        }
                        else if ((S_IXUSR & directoryStat.st_mode) == S_IXUSR)
                        {
                            printf("x");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IRGRP & directoryStat.st_mode) == S_IRGRP){ // group read permission print r
                            printf("r");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IWGRP & directoryStat.st_mode) == S_IWGRP){ // group write permission print w
                            printf("w");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IXGRP & directoryStat.st_mode) == S_IXGRP && (S_ISGID & directoryStat.st_mode) == S_ISGID){ // group exe permission print x
                            printf("s");
                        }
                        else if ((S_IXUSR & directoryStat.st_mode) != S_IXUSR && (S_ISGID & directoryStat.st_mode) == S_ISGID)
                        {
                            printf("S");
                        }
                        else if ((S_IXGRP & directoryStat.st_mode) == S_IXGRP)
                        {
                            printf("x");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IROTH & directoryStat.st_mode) ==  S_IROTH){ // other read permission print r
                        printf("r");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IWOTH & directoryStat.st_mode) == S_IWOTH){ // other write permission print w
                            printf("w");
                        }
                        else{
                            printf("-");
                        }

                        if((S_IXOTH & directoryStat.st_mode) == S_IXOTH){ // other exe permission print x also print space
                            printf("x ");
                        }
                        else{
                            printf("- ");
                        }

                        printf("%-2lu ", directoryStat.st_nlink); // num hard links

                        getAndPrintUserName(directoryStat.st_uid); // user name
                        getAndPrintGroup(directoryStat.st_gid); // group name

                        printf("%5li ", directoryStat.st_size); // size

                        struct tm* modTime = gmtime(&directoryStat.st_mtime);
                        switch (modTime->tm_mon)
                        {
                        case 0:
                            printf("Jan ");
                            break;
                        case 1:
                            printf("Feb ");
                            break;
                        case 2:
                            printf("Mar ");
                            break;
                        case 3:
                            printf("Apr ");
                            break;
                        case 4:
                            printf("May ");
                            break;
                        case 5:
                            printf("Jun ");
                            break;
                        case 6:
                            printf("Jul ");
                            break;
                        case 7:
                            printf("Aug ");
                            break;
                        case 8:
                            printf("Sep ");
                            break;
                        case 9:
                            printf("Oct ");
                            break;
                        case 10:
                            printf("Nov ");
                            break;
                        case 11:
                            printf("Dec ");
                            break;
                        default:
                            break;
                        }
                        printf("%2i %i ", modTime->tm_mday, modTime->tm_year + 1900);
                        printf("%02i:%02i ", modTime->tm_hour, modTime->tm_min);

                        if(hasSpace == true){
                            printf("\'");
                        }
                        printf("%s", name);
                        if(hasSpace == true){
                            printf("\' ");
                        }
                        else{
                            printf(" ");
                        }
                        if(isLink == true){
                            char filePath[PATH_MAX];
                            realpath(fullPath, filePath);
                            printf("-> %s\n", filePath);
                        }
                        else{
                            printf("\n");
                        }
                        fullPath[0] = '\0';
                    }
                    directoryEntry = readdir(directory);
                }
                directoryToPrint = ListTrim(directoryList);
            }
            closedir(directory);
        }
    }
    else if(inode == true){ // print inode number -i
        if(ListCount(directoryList) == 0){
            directory = opendir(".");
            if(directory == NULL){
                printf("ls: cannot access \'.\': No such file or directory\n");
                return -1;
            }
            directoryEntry = readdir(directory);
            while(directoryEntry != NULL){
                bool hasSpace = false;
                char* name = directoryEntry->d_name;
                if(strchr(name, ' ')){
                    hasSpace = true;
                }
                if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                }
                else{
                    if(hasSpace == true){
                        printf("\'");
                    }
                    printf("%-11lu  %s", directoryEntry->d_ino, name);
                    if(hasSpace == true){
                        printf("\'\n");
                    }
                    else{
                        printf("\n");
                    }
                }
                directoryEntry = readdir(directory);
            }
            closedir(directory);
        }
        else{ // directories passed in
            int count = ListCount(directoryList);
            char* directoryToPrint = ListTrim(directoryList);
            while(directoryToPrint != NULL){
                directory = opendir(directoryToPrint);
                if(directory == NULL){ // doesn't exit
                    printf("ls: cannot access \'%s\': No such file or directory\n", directoryToPrint);
                    return -1;
                }
                else{ // does exist
                    if(count != 1){ // only print directory name if more than 1 directory passed in
                        printf("\n");
                        printf("%s:\n", directoryToPrint);
                    }

                    directoryEntry = readdir(directory);
                    while(directoryEntry != NULL){
                        bool hasSpace = false;
                        char* name = directoryEntry->d_name;
                        if(strchr(name, ' ')){
                            hasSpace = true;
                        }
                        if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                        }
                        else{
                            printf("%-11lu  ",directoryEntry->d_ino);
                            if(hasSpace == true){
                                printf("\'");
                            }
                            printf("%s", name);
                            if(hasSpace == true){
                                printf("\'\n");
                            }
                            else
                            {
                                printf("\n");
                            }
                        }
                        directoryEntry = readdir(directory);
                    }
                }
                directoryToPrint = ListTrim(directoryList);
            }
            closedir(directory);
        }
        
    }
    else{ // print directories either current or whichever passed in
        if(ListCount(directoryList) == 0){ // 0 directories were passed in, print current directory contents
            directory = opendir(".");
            if(directory == NULL){
                printf("ls: cannot access \'.\': No such file or directory\n");
                return -1;
            }
            directoryEntry = readdir(directory);
            while(directoryEntry != NULL){
                bool hasSpace = false;
                char* name = directoryEntry->d_name;
                if(strchr(name, ' ')){
                    hasSpace = true;
                }
                if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                }
                else{
                    if(hasSpace == true){
                        printf("\'");
                    }
                    printf("%s", name);
                    if(hasSpace == true){
                        printf("\'\n");
                    }
                    else{
                        printf("\n");
                    }
                }
                directoryEntry = readdir(directory);
            }
            closedir(directory);
        }
        else{ // directories passed in
            int count = ListCount(directoryList);
            char* directoryToPrint = ListTrim(directoryList);
            while(directoryToPrint != NULL){
                directory = opendir(directoryToPrint);
                if(directory == NULL){ // doesn't exit
                    printf("ls: cannot access \'%s\': No such file or directory\n", directoryToPrint);
                    return -1;
                }
                else{ // does exist
                    if(count != 1){ // only print directory name if more than 1 directory passed in
                        printf("\n");
                        printf("%s:\n", directoryToPrint);
                    }

                    directoryEntry = readdir(directory);
                    while(directoryEntry != NULL){
                        bool hasSpace = false;
                        char* name = directoryEntry->d_name;
                        if(strchr(name, ' ')){
                            hasSpace = true;
                        }
                        if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0 || name[0] == '.'){ // hidden files
                    
                        }
                        else{
                            if(hasSpace == true){
                                printf("\'");
                            }
                            printf("%s", name);
                            if(hasSpace == true){
                                printf("\'\n");
                            }
                            else
                            {
                                printf("\n");
                            }
                        }
                        directoryEntry = readdir(directory);
                    }
                }
                directoryToPrint = ListTrim(directoryList);
            }
            closedir(directory);
        }
    }

    ListFree(directoryList, &freeDirectory);
    
}

void freeDirectory(void* toFree){
    return;
}

void getAndPrintUserName(uid_t uid) {

  struct passwd *pw = NULL;
  pw = getpwuid(uid);

  if (pw) {
    printf("%s ",pw->pw_name);
  } else {
    printf("ls: No user found for %u\n", uid);
  }
}

void getAndPrintGroup(gid_t grpNum) {
  struct group *grp;

  grp = getgrgid(grpNum); 
  
  if (grp) {
    printf("%s ", grp->gr_name);
  } else {
    printf("ls: No group name for %u found\n", grpNum);
  }
}

/*char path[500] = {0};
    int dest_len = 500;
    if (readlink ("/proc/self/exe", path, dest_len) != -1){
        dirname (path);
        strcat  (path, "/");
        printf("path=%s\n", path);
    }*/