#ifndef UNIXLS_H
#define UNIXLS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <sys/sysmacros.h>
#include <limits.h>
#include "list.h"

void freeDirectory(void* toFree);

void getAndPrintUserName(uid_t uid);

void getAndPrintGroup(gid_t grpNum);

#endif