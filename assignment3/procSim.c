#include "procSim.h"

// contants for process blocked, ready, running
#define blocked -1
#define ready 0
#define running 1

//struct for process
// -status
// -id
// -string for update
// -priority
struct PCB{
    int pid;
    int status;
    int priority;
    char message[70];
    int SRR; // send recieve reply
};

#define Nothing 0
#define WaitingReply 1
#define WaitingSend 2
#define WaitingRecieve 3
#define PrintReply 4
#define PrintMessage 5

// current process running
struct PCB* runningProc;

// init process pointer so no need to search for it in the list
struct PCB* initProc;

// list of ready processes, blocked processes
struct LIST* readyProcsPri0;
struct LIST* readyProcsPri1;
struct LIST* readyProcsPri2;
struct LIST* blockedProcs;
struct LIST* allProcs;

//struct for semaphore and list of processes blocked on it
// only 0-4 (5) sems allowed
struct semStruct{
    int semID;
    int value;
    struct LIST* blockedList;
};

struct semStruct* sem0;
struct semStruct* sem1;
struct semStruct* sem2;
struct semStruct* sem3;
struct semStruct* sem4;

struct semStruct* semArray[5];

// used to give new processes a pid
int procCounter;

bool exitSim;

int main(){
    exitSim = false;
    procCounter = 0;
    readyProcsPri0 = ListCreate();
    if(readyProcsPri0 == NULL){
        printf("Creating list of ready proceses with priority 0 failed\n");
        return -1;
    }
    readyProcsPri1 = ListCreate();
    if(readyProcsPri1 == NULL){
        printf("Creating list of ready proceses with priority 1 failed\n");
        return -1;
    }
    readyProcsPri2 = ListCreate();
    if(readyProcsPri2 == NULL){
        printf("Creating list of ready proceses with priority 2 failed\n");
        return -1;
    }
    blockedProcs = ListCreate();
    if(blockedProcs == NULL){
        printf("Creating list of blocked proceses failed\n");
        return -1;
    }
    allProcs = ListCreate();
    if(allProcs == NULL){
        printf("Creating list of blocked proceses failed\n");
        return -1;
    }
    
    // initalize the sems to NULL as NewSem has not been called
    for(int i = 0; i <= 4; i++){
        semArray[i] = NULL;
    }

    // set up init proc
    CreateProc(0);
    printf("Successfully created init process with pid: %i\n", procCounter - 1);

    // set up running proc to be init
    runningProc = ListFirst(allProcs);
    initProc = runningProc;
    
    char command[50];
    while (true)
    {
        if(exitSim == true){
            break;
        }
        printf("\n\n\nEnter command character: ");
        scanf("%s", command);

        if(strlen(command) > 1){
          printf("Command character too long, try again\n");
          continue;
        }
        switch(toupper(command[0])){
            case 'C':
            {
                int priority;
                printf("Enter priority of new process: ");
                scanf("%d", &priority);
                if(priority < 0 || priority > 2){
                    printf("Failure to create process due to incorrect priority value\n");
                    break;
                }
                CreateProc(priority);
                // print info
                printf("Successfully created process with pid: %i\n", procCounter - 1);
                break;
            }
            case 'F':
                if(ForkProc() == -1){
                    printf("Failure to fork process\n");
                }
                else{
                    printf("Successfully forked process with pid: %i\n", procCounter - 1);
                }
                break;
            case 'K':{
                int killPID;
                bool killRunningProc;
                printf("Enter pid of the process to kill: ");
                scanf("%i", &killPID);
                killRunningProc = (killPID == runningProc->pid) ? true : false;
                int result = KillProc(killPID);
                if(exitSim == true){ // exit simulation
                    break;
                }
                if(result == -1){
                    printf("Failed to kill process with pid: %i\n", killPID);
                }
                else{
                    if(killRunningProc == true){
                        printf("Sucessfuly killed process with pid: %i, New process running with pid: %i\n", killPID, runningProc->pid);
                        if(runningProc->SRR == PrintMessage || runningProc->SRR == PrintReply){
                            printf("Message: %s\n", runningProc->message);
                            runningProc->message[0] = '\0';
                            runningProc->SRR = Nothing;
                        }
                    }
                    else{
                        printf("Sucessfuly killed process with pid: %i\n", killPID);
                    }
                }
                break;
            }
            case 'E':{
                int result = KillProc(runningProc->pid);
                if(exitSim == true){
                    break;
                }
                if(result == -1){
                    printf("Failed to kill running process with pid: %i\n", runningProc->pid);
                }
                else{
                    printf("Sucessfuly killed process, New process running with pid: %i\n", runningProc->pid);
                        if(runningProc->SRR == PrintMessage || runningProc->SRR == PrintReply){
                            printf("Message: %s\n", runningProc->message);
                            runningProc->message[0] = '\0';
                            runningProc->SRR = Nothing;
                        }
                }
                break;
            }
            case 'Q':{
                int oldPID = runningProc->pid;
                QuantumSim();
                if(runningProc->pid == oldPID){
                    printf("Time quantum complete, same process running with pid: %i\n", runningProc->pid);
                }
                else{
                    printf("Time quantum complete, new process running with pid: %i\n", runningProc->pid);
                }
                if(runningProc->SRR == PrintMessage || runningProc->SRR == PrintReply){
                    printf("Message: %s\n", runningProc->message);
                    runningProc->message[0] = '\0';
                    runningProc->SRR = Nothing;
                }
                break;
            }
            case 'S':{
                int oldPID = runningProc->pid;
                int sendPID;
                char message[40];
                printf("Enter pid of the process to send the message to: ");
                scanf("%i", &sendPID);
                printf("Enter message to send, max 40 characters:\n");
                scanf("%s", message);
                int result = SendMessage(sendPID, message);
                if(result == -1){
                    printf("Failed to send message to process with pid: %i\n", sendPID);
                }
                else{
                    printf("Sucessfully sent message to process with pid: %i\n", sendPID);
                    if(runningProc->pid == oldPID){ // init will not block and keep running
                        printf("Process init kept running with pid: %i\n", runningProc->pid);
                    }
                    else{ // new process running now
                        printf("New process running with pid: %i\n", runningProc->pid);
                    }
                    if(runningProc->SRR == PrintMessage || runningProc->SRR == PrintReply){
                        printf("Message: %s\n", runningProc->message);
                        runningProc->message[0] = '\0';
                        runningProc->SRR = Nothing;
                    }
                }
                break;
            }
            case 'R':{
                int oldPID = runningProc->pid;
                printf("Recieving message\n");
                RecieveMessage();
                if(runningProc->pid == oldPID){ // init will not block and keep running
                    printf("Process kept running with pid: %i\n", runningProc->pid);
                }
                else{ // new process running now
                    printf("New process running with pid: %i\n", runningProc->pid);
                }
                if(runningProc->SRR == PrintMessage || runningProc->SRR == PrintReply){
                    printf("Message: %s\n", runningProc->message);
                    runningProc->message[0] = '\0';
                    runningProc->SRR = Nothing;
                }
                break;
            }
            case 'Y':{
                int replyPID;
                char message[40];
                printf("Enter pid of the process to reply the message to: ");
                scanf("%i", &replyPID);
                printf("Enter message to send, max 40 characters:\n");
                scanf("%s", message);
                int result = ReplyMessage(replyPID, message);
                if(result == -1){
                    printf("Failed to reply message to process with pid: %i\n", replyPID);
                }
                else if(result == 0){
                    printf("Attempted a reply message to process with pid: %i but it was not expecting a reply\n", replyPID);
                }
                else{ 
                    printf("Sucessfully replied message to process with pid: %i\n", replyPID);
                }
                break;
            }
            case 'N':{
                int semID;
                int initValue;
                printf("Enter id of new semaphore between 0 and 4: ");
                scanf("%i", &semID);
                printf("Enter inital value to new semaphore: ");
                scanf("%i", &initValue);
                int result = NewSem(semID, initValue);
                if(result == -1){
                    printf("Failed to create new semaphore\n");
                }
                else{
                    printf("Sucessfully created new semaphore with id: %i, it has value: %i", semID, semArray[semID]->value);
                }
                break;
            }
            case 'P':{
                int semID;
                printf("Enter id of semaphore to P: ");
                scanf("%i", &semID);
                int result = PSem(semID);
                if(result == -1){
                    printf("Failed to P semaphore with id: %i\n", semID);
                }
                else{
                    printf("Sucessfully P semaphore with id: %i, it has value: %i", semID, semArray[semID]->value);
                    if(runningProc->SRR == PrintMessage || runningProc->SRR == PrintReply){
                        printf("Message: %s\n", runningProc->message);
                        runningProc->message[0] = '\0';
                        runningProc->SRR = Nothing;
                    }
                }
                break;
            }
            case 'V':{
                int semID;
                printf("Enter id of semaphore to V: ");
                scanf("%i", &semID);
                int result = VSem(semID);
                if(result == -1){
                    printf("Failed to V semaphore with id: %i\n", semID);
                }
                else{
                    printf("Sucessfully V semaphore with id: %i, it has value: %i", semID, semArray[semID]->value);
                }
                break;
            }
            case 'I':{
                int infoPID;
                printf("Enter pid of process to retrieve info: ");
                scanf("%i", &infoPID);
                ProcInfo(infoPID);
                break;
            }
            case 'T':
                TotalInfo();
                break;
            default:
                printf("Command character invalid, try again\n");
                //continue;
                break;
        }
    }
    
    
    ListFree(allProcs, &freePCB);
    ListFree(readyProcsPri0, &freePCB);
    ListFree(readyProcsPri1, &freePCB);
    ListFree(readyProcsPri2, &freePCB);
    ListFree(blockedProcs, &freePCB);

    for(int i = 0; i < 5; i++){ // free sem lists if they were initialized
        if(semArray[i] != NULL){
            ListFree(semArray[i]->blockedList, &freeSem);
            free(semArray[i]);
        }
    }

    printf("Exiting simulation\n");
    return 1;
}

void freePCB(void* toFree){
    free(toFree);
    return;
}

void freeSem(void* toFree){
    return;
}

bool ComparePCB(void* SearchPCB, void* findPID){
    struct PCB* FindPCB = (struct PCB*)SearchPCB;
    if(FindPCB->pid == *(int*)findPID){
        return true;
    }
    else{
        return false;
    }
}

int CreateProc(int priority){
    // create and setup new PCB
    struct PCB* newPCB = malloc(sizeof(struct PCB));
    newPCB->pid = procCounter;
    newPCB->priority = priority;
    newPCB->SRR = Nothing;
    if(procCounter == 0){ // if creating the init process then set it to running
        newPCB->status = running;
    }
    else{ // otherwise it is ready
        newPCB->status = ready;
        if (priority == 0)
        {
            ListAppend(readyProcsPri0, newPCB);
        }
        else if(priority == 1){
            ListAppend(readyProcsPri1, newPCB);
        }
        else{
            ListAppend(readyProcsPri2, newPCB);
        }
    }
    ListAppend(allProcs, newPCB); // add to list of all processes

    // increment for the next process created
    procCounter++;
    return 1;
}

int ForkProc(){
    if(runningProc->pid == 0){
        return -1;
    }
    //ListFirst(allProcs); // set current pointer to beginning of the list
    //struct PCB* procToFork = ListSearch(allProcs, &ComparePCB, &(runningProc->pid)); // searching for the running proc, it must be exist
    CreateProc(runningProc->priority);
    return 1;
}

int KillProc(int killPID){
    if(killPID == 0){
        if(ListCount(allProcs) == 1){
            exitSim = true;
        }
        return -1;
    }

    if(killPID < 0 || killPID >= procCounter){ // killPID is outside the current range of pid
        return -1;
    }

    for(int i = 0; i < 5; i++){ // cannot kill a process that is blocked on a semaphore
        if(semArray[i] != NULL){ // semaphore is initialized
            // if found return -1
            ListFirst(semArray[i]->blockedList);
            if(ListSearch(semArray[i]->blockedList, &ComparePCB, &killPID) != NULL){
                printf("Cannot kill a process blocked on a semaphore\n");
                return -1;
            }
        }
    }

    ListFirst(allProcs); // set current pointer to beginning of the list
    struct PCB* procToKill = ListSearch(allProcs, &ComparePCB, &killPID); 

    if(procToKill == NULL){ // might have been previously killed/exited already
        return -1;
    }
    ListRemove(allProcs);
    
    ListFirst(blockedProcs); // set current pointer to beginning of the list
    if(ListSearch(blockedProcs, &ComparePCB, &killPID) != NULL){
        ListRemove(blockedProcs);
    }
    else{
        if(procToKill->priority == 0){
            ListFirst(readyProcsPri0); // set current pointer to beginning of the list
            if(ListSearch(readyProcsPri0, &ComparePCB, &killPID) != NULL){
                ListRemove(readyProcsPri0);
            }
        }
        else if(procToKill->priority == 1){
            ListFirst(readyProcsPri1); // set current pointer to beginning of the list
            if(ListSearch(readyProcsPri1, &ComparePCB, &killPID) != NULL){
                ListRemove(readyProcsPri1);
            }
        }
        else{
            ListFirst(readyProcsPri2); // set current pointer to beginning of the list
            if(ListSearch(readyProcsPri2, &ComparePCB, &killPID) != NULL){
                ListRemove(readyProcsPri2);
            }
        }
    }
    // if the killed process is the current running process need to update running
    if(killPID == runningProc->pid){
        if(ListCount(readyProcsPri0) != 0){
            runningProc = ListFirst(readyProcsPri0);
            ListRemove(readyProcsPri0);
        }
        else if(ListCount(readyProcsPri1) != 0){
            runningProc = ListFirst(readyProcsPri1);
            ListRemove(readyProcsPri1);
        }
        else if(ListCount(readyProcsPri2) != 0){
            runningProc = ListFirst(readyProcsPri2);
            ListRemove(readyProcsPri2);
        }
        else{
            runningProc = initProc;
        }
        runningProc->status = running;
    }

    //free the dynamic memory now that the process has been killed/exited
    free(procToKill);
    return 1;
}

void QuantumSim(){
    // if running process is init then just update status find a new process, otherwise add it to the end of the appropriate ready list
    if(runningProc != initProc){ 
        if(runningProc->priority == 0){
            ListAppend(readyProcsPri0, runningProc);
        }
        else if(runningProc->priority == 1){
            ListAppend(readyProcsPri1, runningProc);
        }
        else if(runningProc->priority == 2){
            ListAppend(readyProcsPri2, runningProc);
        }
        runningProc->status = ready;
    }
    else{
        initProc->status = ready;
    }
    
    // get the new process to run, if running process is the only process then it will get chosen again
    if(ListCount(readyProcsPri0) != 0){
        runningProc = ListFirst(readyProcsPri0);
        ListRemove(readyProcsPri0);
    }
    else if(ListCount(readyProcsPri1) != 0){
        runningProc = ListFirst(readyProcsPri1);
        ListRemove(readyProcsPri1);
    }
    else if(ListCount(readyProcsPri2) != 0){
        runningProc = ListFirst(readyProcsPri2);
        ListRemove(readyProcsPri2);
    }
    else{
        runningProc = initProc; // this should never happen as a process was just added to one of the lists unless init is running
    }
    runningProc->status = running;
    return;
}

int SendMessage(int sendPID, char* message){
    if(sendPID < 0 || sendPID >= procCounter){ // pid out of range or message too long
        printf("Invalid pid to send message\n");
        return -1;
    }
    if(strlen(message) > 40){
        printf("Message too long, max 40 characters\n");
        return -1;
    }
    if(sendPID == runningProc->pid){
        printf("Not allowed to send a message to the same process that is running\n");
        return -1;
    }

    ListFirst(allProcs); // set current pointer to beginning of the list
    struct PCB* sendPCB = ListSearch(allProcs, &ComparePCB, &sendPID);
    if(sendPCB == NULL){ // process had been killed/exited already
        return -1;
    }

    if(sendPCB->SRR == WaitingSend){ // process is waiting for a send so unblock it and set "Recieved: " and block this process
        if(sendPCB != initProc){ // blocked after doing a recieve, get from blocked list and put on correct ready list
            ListFirst(blockedProcs); // set current pointer to beginning of the list
            // should always be found, only way to have "WaitingSend: " is to have done a recieve and not be init
            ListSearch(blockedProcs, &ComparePCB, &sendPCB->pid);
            ListRemove(blockedProcs); // remove from blocked process list
            
            // add it to the correct ready list
            if(sendPCB->priority == 0){
                ListAppend(readyProcsPri0, sendPCB);
            }
            else if(sendPCB->priority == 1){
                ListAppend(readyProcsPri1, sendPCB);
            }
            else{
                ListAppend(readyProcsPri2, sendPCB);
            }
        }
        strcpy(sendPCB->message, message);
        sprintf(sendPCB->message + strlen(sendPCB->message), " (Sent by pid = %d)", runningProc->pid);
        sendPCB->SRR = PrintMessage; // message recieved and ready to print
        sendPCB->status = ready; // update status of the process recieving a message to be ready to run now
    }
    else{ // the other process was not expecting a send so set the SSR and block
        strcpy(sendPCB->message, message);
        sprintf(sendPCB->message + strlen(sendPCB->message), " (Sent by pid = %d)", runningProc->pid);
        sendPCB->SRR = WaitingRecieve; // process has recieved a message, needs to do a recieve
    }

    if(runningProc != initProc){ // process will be blocked so find replacement 
        runningProc->status = blocked;
        runningProc->SRR = WaitingReply;
        ListAppend(blockedProcs, runningProc); // add running process to the blocked list

        if(ListCount(readyProcsPri0) != 0){
            runningProc = ListFirst(readyProcsPri0);
            ListRemove(readyProcsPri0);
        }
        else if(ListCount(readyProcsPri1) != 0){
            runningProc = ListFirst(readyProcsPri1);
            ListRemove(readyProcsPri1);
        }
        else if(ListCount(readyProcsPri2) != 0){
            runningProc = ListFirst(readyProcsPri2);
            ListRemove(readyProcsPri2);
        }
        else{
            runningProc = initProc; // only happens if everything is blocked/empty
        }
        runningProc->status = running;
    }
    else{ // will not block but show intent of waiting for reply
        runningProc->SRR = WaitingReply;
    }
    return 1;
}

void RecieveMessage(){
    if(runningProc->SRR == WaitingRecieve){ // will not block as message is waiting to be recieved
        runningProc->SRR = PrintMessage;
    }
    else{
        if(runningProc != initProc){ // will block while waiting to recieve
            runningProc->status = blocked;
            runningProc->SRR = WaitingSend;
            ListAppend(blockedProcs, runningProc); // add running process to the blocked list

            if(ListCount(readyProcsPri0) != 0){
                runningProc = ListFirst(readyProcsPri0);
                ListRemove(readyProcsPri0);
            }
            else if(ListCount(readyProcsPri1) != 0){
                runningProc = ListFirst(readyProcsPri1);
                ListRemove(readyProcsPri1);
            }
            else if(ListCount(readyProcsPri2) != 0){
                runningProc = ListFirst(readyProcsPri2);
                ListRemove(readyProcsPri2);
            }
            else{
                runningProc = initProc; // only happens if everything is blocked/empty
            }
            runningProc->status = running;
        }
        else{ // will not block but show intent to get message
            runningProc->SRR = WaitingSend;
        }
    }
    return;
}

int ReplyMessage(int replyPID, char* message){
    if(replyPID < 0 || replyPID >= procCounter){ // pid out of range or message too long
        printf("Invalid pid to send message\n");
        return -1;
    }
    if(strlen(message) > 40){
        printf("Message too long, max 40 characters\n");
        return -1;
    }

    ListFirst(allProcs); // set current pointer to beginning of the list
    struct PCB* replyPCB = ListSearch(allProcs, &ComparePCB, &replyPID);
    if(replyPCB == NULL){ // process had been killed/exited already
        return -1;
    }

    if(replyPCB->SRR == WaitingReply){ // reply only unblocks a process blocked on send
        if(replyPCB != initProc){ // init process will never be blocked, take off block list and add to correct ready list
            ListFirst(blockedProcs);
            ListSearch(blockedProcs, &ComparePCB, &replyPCB->pid);
            ListRemove(blockedProcs);

            // add it to the correct ready list
            if(replyPCB->priority == 0){
                ListAppend(readyProcsPri0, replyPCB);
            }
            else if(replyPCB->priority == 1){
                ListAppend(readyProcsPri1, replyPCB);
            }
            else{
                ListAppend(readyProcsPri2, replyPCB);
            }

            replyPCB->status = ready;
        }
        strcpy(replyPCB->message, message);
        sprintf(replyPCB->message + strlen(replyPCB->message), " (replied by pid = %d)", runningProc->pid);
        replyPCB->SRR = PrintReply;
        return 1;
    }
    else{
        return 0;
    }
}

int NewSem(int semID, int initValue){
    if(semID < 0 || semID > 4){
        printf("Invalid id of new semaphore\n");
        return -1;
    }
    if(semArray[semID] != NULL){ // has already been initialized
        printf("Semaphore has already been initialized\n");
        return -1;
    }

    struct semStruct* semInit = malloc(sizeof(struct semStruct)); 
    semArray[semID] = semInit;
    semInit->semID = semID;
    semInit->value = initValue;
    semInit->blockedList = ListCreate();
    return 1;
}

int PSem(int semID){
    if(semID < 0 || semID > 4){
        printf("Invalid id of new semaphore\n");
        return -1;
    }
    if(semArray[semID] == NULL){
        printf("Semaphore has not been initialized\n");
        return -1;
    }
    if(runningProc == initProc){
        printf("Cannot call P on the init process\n");
        return -1;
    }

    struct semStruct* semToP = semArray[semID];
    semToP->value -= 1;
    if(semToP->value < 0){ // add running process to semaphores blocked list and find replacement
        runningProc->status = blocked;
        ListAppend(semToP->blockedList, runningProc);

        // find replacement
        if(ListCount(readyProcsPri0) != 0){
            runningProc = ListFirst(readyProcsPri0);
            ListRemove(readyProcsPri0);
        }
        else if(ListCount(readyProcsPri1) != 0){
            runningProc = ListFirst(readyProcsPri1);
            ListRemove(readyProcsPri1);
        }
        else if(ListCount(readyProcsPri2) != 0){
            runningProc = ListFirst(readyProcsPri2);
            ListRemove(readyProcsPri2);
        }
        else{
            runningProc = initProc; // no other process to run
        }
        runningProc->status = running;
    }

    return 1;
}

int VSem(int semID){
    if(semID < 0 || semID > 4){
        printf("Invalid id of new semaphore\n");
        return -1;
    }
    if(semArray[semID] == NULL){
        printf("Semaphore has not been initialized\n");
        return -1;
    }

    struct semStruct* semToV = semArray[semID];
    semToV->value += 1;
    if(semToV->value <= 0){ // unblock and add to ready list
        if(ListCount(semToV->blockedList) != 0){
            struct PCB* unblockPCB = ListFirst(semToV->blockedList);
            ListRemove(semToV->blockedList);
            unblockPCB->status = ready;

            // add it to the correct ready list
            if(unblockPCB->priority == 0){
                ListAppend(readyProcsPri0, unblockPCB);
            }
            else if(unblockPCB->priority == 1){
                ListAppend(readyProcsPri1, unblockPCB);
            }
            else{
                ListAppend(readyProcsPri2, unblockPCB);
            }
        }
    }

    return 1;
}

void ProcInfo(int infoPID){
    if(infoPID == initProc->pid){ // efficently print the init process
        printf("\npid: %i\n", infoPID);
        if(runningProc->pid == infoPID){
            printf("init is currently running\n");
        }

        // print message
        printf("Internal Message: ");
        if(strlen(initProc->message) != 0){ // there is a message waiting to be displayed
            printf("%s\n", initProc->message);
        }
        else{
            printf("Empty\n"); // no message
        }

        if(initProc->SRR == WaitingRecieve){
            printf("Message available to Recieve\n");
        }

        printf("This is a special init process, it will run when there is no other ready process (lowest priority) and will never block.\n");
        printf("Calling Kill/Exit when init is the only process in the simulation will end the simulation.\n");

        return;
    }

    ListFirst(allProcs); // set current pointer to beginning of the list
    struct PCB* infoPCB = ListSearch(allProcs, &ComparePCB, &infoPID);
    if(infoPCB == NULL){
        printf("There is no process with pid: %i\n", infoPID);
        return;
    }
    else{
        printf("\npid: %i\n", infoPID);
        char statusPrint[10];
        if(infoPCB->status == blocked){
            strcpy(statusPrint, "Blocked");
        }
        else if(infoPCB->status == ready){
            strcpy(statusPrint, "Ready");
        }
        else if(infoPCB->status == running){
            strcpy(statusPrint, "Running");
        }

        printf("Priority: %i, Status: %s, Internal Message: ", infoPCB->priority, statusPrint);
        if(strlen(infoPCB->message) != 0){ // there is a message waiting to be displayed
            printf("%s\n", infoPCB->message);
        }
        else{
            printf("Empty\n"); // no message
        }

        if(infoPCB->SRR == WaitingRecieve){
            printf("Message available to Recieve\n");
        }
        else if(infoPCB->SRR == WaitingReply){
            printf("Waiting for a reply\n");
        }
        else if(infoPCB->SRR == WaitingSend){
            printf("Waiting to recieve\n");
        }
    }
    return;
}

void TotalInfo(){
    // print num of processes
    printf("Printing info on all processes\nThere are currently %i process(es) in total\n", ListCount(allProcs));
    struct PCB* printPCB = ListFirst(allProcs); // first process in the list of all processes, the init process

    while(printPCB != NULL){ // call procInfo on each process in the list of all processes
        ProcInfo(printPCB->pid);
        printPCB = ListNext(allProcs);
    }

    // print semaphore info if it has been initialized
    for(int i = 0; i < 5; i++){
        if(semArray[i] != NULL){
            printf("\nSemaphore with id: %i has value: %i\n", i, semArray[i]->value);
        }
    }

    return;
}