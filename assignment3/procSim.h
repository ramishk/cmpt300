#ifndef S_TALK
#define S_TALK

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "list.h"

int CreateProc(int priority);
// Creates a new process and reports the new pid, report success/failure

int ForkProc();
// Forks current process into a new process and reports the new pid, report success/failure

int KillProc(int killPID);
// End process with pid = killPID, report success/failure

void ExitProc();
// End the current running process, print info of new process running

void QuantumSim();
// Quantum passed for current running process, print info of new process running

int SendMessage(int sendPID, char* message);
// Send message, max 40, to pid = sendPID, blocks until a reply is received. Print info of new process running. Print reply when it is received

void RecieveMessage();
// Receive message on current process, blocks until a send is received. Print info of new process running. Print sent message when is is received

int ReplyMessage(int replyPID, char* message);
// Reply message, max 40, to pid = replyPID, unblocks the sender

int NewSem(int semID, int initValue);
// Report if sem was created, report success/failure

int PSem(int semID);
// Decrement sem = semID if exists and if process was blocked or not, report success/failure

int VSem(int semID);
// Increment sem = semID if exists and if which process was unblocked, report success/failure

void ProcInfo(int infoPID);
// Print info of pid = pid

void TotalInfo();
// Print info on all process

void freePCB(void* toFree);
// used in ListFree to free PCB

void freeSem(void* toFree);
// used in ListFree to free Sem

bool ComparePCB(void* SearchPCB, void* findPID);
// first argument a PCB pointer, and as its second argument pid to compare.

#endif