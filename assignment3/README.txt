- Can enter command either lower or uppercase

- Can send, recieve, reply to and from init but it will not block ever

- When calling kill/exit with init currently running I choose to not allow a new ready process to run so that the person would have more control over the simulation usign Quantum.

- Sending a reply to a process and then the process that recieved a reply does a send it will be blocked as it makes no sense for a reply to come before a send. Essentially sending a reply to a process not blocked on a send will do nothing.

- No mailbox feature

- Sending a message to youself makes no sense so is not allowed

- Killing a process that is blocked on a semaphore is not allowed, user must unblock it and then kill it

- Cannot call P(sem) on init process as that makes no sense, init will not block

- Starvation is possible and up to the user to control